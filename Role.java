package domain;

import java.util.List;

public class Role {
	private List<Permission> perm;

	public List<Permission> getPerm() {
		return perm;
	}

	public void setPerm(List<Permission> perm) {
		this.perm = perm;
	}
	
}
